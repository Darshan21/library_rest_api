//exported json


package main

import (
        "fmt"
          "encoding/json"
          "net/http"
          )

          type Person struct{
          	First string
          	Last string
          	Age int
          	NotExported  int
          }

          func booksDispalyer(rw http.ResponseWriter, req *http.Request){
          		rw.Write([]byte("Hello Go programmers"));
          }

          func main(){
          	p1:=Person{"darshan","sidar",22,007}
          	bs, _:=json.Marshal(p1) 
          	fmt.Println(bs)       //bs-byte size
          	fmt.Printf("%T\n",bs)
          	fmt.Println(string(bs))

          	http.HandleFunc("/library/books/programming/all",booksDispalyer);
          	http.ListenAndServe(":9000",nil)
          }